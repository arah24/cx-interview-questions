module.exports = {
    transform: {
      '^.+\\.[jt]sx?$': 'ts-jest',
    },
    testEnvironment: 'node',
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
    "roots": [
        "<rootDir>/src"
    ]
  }
  