# ECS Shopping Basket

Shopping basket for supermarket

Please execute the following commands to run the app:

## Getting Started

These instructions should get this project working locally on your machine.

### Prerequisites

```
npm
yarn
node
```

### Installation

clone the project into a directory then cd into it and run:
```
yarn install
```
if you get network issues from yarn install, please run `npm install` instead.

### Running the app

```
yarn run start
```

You should get the follow output.

```
{ subtotal: '£14.91', discount: '£6.66', total: '£8.25' }
```

### Modifications

- change the contents under `index.ts` to establish a new basket and catalogue

### Bonus

No bonus question was attempted due to time constraint
