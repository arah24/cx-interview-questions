import { createBasket, createCatalogue } from '../factory/factory'
import {
  BUY_TWO_GET_ONE_FREE,
  TWENTY_FIVE_PERFECT_OFF,
} from '../util/constants'
import {
  Item,
  Result,
  CatalogueProducts,
  CatalogueOffers,
  Basket,
  Catalogue,
} from '../types/types'

import { calculatePrice } from './basketPricer'
import { getPricePerProduce } from './priceService'

describe('basket pricer service', () => {
  it('should return £0 for empty basket', () => {
    const basket: Basket = createBasket()

    const products: CatalogueProducts = {}
    const offers: CatalogueOffers = {}
    const superMarket: Catalogue = createCatalogue(products, offers)

    const expectedResult: Result = {
      subtotal: '£0.00',
      discount: '£0.00',
      total: '£0.00',
    }
    expect(
      calculatePrice(getPricePerProduce, { basket, catalogue: superMarket })
    ).toEqual(expectedResult)
  })
  it('should return sub-total, discount and total', () => {
    const basket: Basket = createBasket()
    basket.addProduct({ productName: 'Baked Beans', quantity: 4 })
    basket.addProduct({ productName: 'Biscuits', quantity: 1 })

    const products: CatalogueProducts = {
      bakedbeans: 0.99,
      biscuits: 1.2,
      sardines: 1.89,
      shampoosmall: 2.0,
      shampoomedium: 2.5,
      shampoolarge: 3.5,
    }
    const offers: CatalogueOffers = {
      bakedbeans: BUY_TWO_GET_ONE_FREE,
      sardines: TWENTY_FIVE_PERFECT_OFF,
    }

    const superMarket: Catalogue = createCatalogue(products, offers)

    const expectedResult: Result = {
      subtotal: '£5.16',
      discount: '£0.99',
      total: '£4.17',
    }

    expect(
      calculatePrice(getPricePerProduce, { basket, catalogue: superMarket })
    ).toEqual(expectedResult)
  })
})
