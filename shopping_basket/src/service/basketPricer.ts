import { Basket, Catalogue, Result } from './../types/types'
import { priceFormat } from '../util/priceUtil'

/**
 * Calculate price for each basket and catalogue
 * @param priceFunc Function that calculates price for basket/catalogue
 * @param { Basket, catalogue }  contains basket and catalogue
 * @returns { subtotal, discount, total } in GBP
 */
const calculatePrice = (
  priceFunc: Function,
  { basket, catalogue }: { basket: Basket; catalogue: Catalogue }
): Result => {
  let subtotal = 0
  let discount = 0
  let total = 0

  priceFunc(basket.getProducts(), catalogue).forEach(
    (product: { subtotal: number; discount: number; total: number }) => {
      subtotal += product.subtotal
      discount += product.discount
      total += product.total
    }
  )
  return {
    subtotal: priceFormat.format(subtotal),
    discount: priceFormat.format(discount),
    total: priceFormat.format(total),
  }
}

export { calculatePrice }
