import { createBasket, createCatalogue } from '../factory/factory'
import {
  Basket,
  CatalogueProducts,
  CatalogueOffers,
  Catalogue,
  Item,
} from '../types/types'
import {
  BUY_TWO_GET_ONE_FREE,
  TWENTY_FIVE_PERFECT_OFF,
} from '../util/constants'
import { applyOffer, getPricePerProduce } from './priceService'

describe('pricerService', () => {
  describe('applyOffer', () => {
    it('calculates the offer for Buy 2 get 1 free with even quanitities', () => {
      const offerType = 'Buy 2 get 1 Free'
      const price = 0.99
      const quantityFor4 = 4
      expect(applyOffer(offerType, quantityFor4, price)).toEqual(2.97)

      const quantityFor6 = 6
      expect(applyOffer(offerType, quantityFor6, price)).toEqual(3.96)

      const quantityFor8 = 8
      expect(applyOffer(offerType, quantityFor8, price)).toEqual(4.95)
    })
    it('calculates the offer for Buy 2 get 1 free with odd quantities', () => {
      const offerType = 'Buy 2 get 1 Free'
      const price = 0.99
      const quantity = 5
      expect(applyOffer(offerType, quantity, price)).toEqual(3.96)
    })
  })
  describe('getPricePerProduce', () => {
    it('should apply the correct price with offers for the correct product and none for the product with no offer.', () => {
      const basket: Basket = createBasket()
      basket.addProduct({ productName: 'Baked Beans', quantity: 4 })
      basket.addProduct({ productName: 'Biscuits', quantity: 1 })

      const products: CatalogueProducts = {
        bakedbeans: 0.99,
        biscuits: 1.2,
        sardines: 1.89,
      }
      const offers: CatalogueOffers = {
        bakedbeans: BUY_TWO_GET_ONE_FREE,
      }
      const superMarket: Catalogue = createCatalogue(products, offers)

      const items: Item[] = basket.getProducts()
      const expected = [
        {
          subtotal: 3.96,
          discount: 0.99,
          total: 2.97,
        },
        {
          subtotal: 1.2,
          discount: 0,
          total: 1.2,
        },
      ]

      expect(getPricePerProduce(items, superMarket)).toEqual(expected)
    })
    it('should applies the correct discount for the corresponding items', () => {
      const basket = createBasket()
      basket.addProduct({ productName: 'Baked Beans', quantity: 6 })
      basket.addProduct({ productName: 'Sardines', quantity: 2 })

      const products: CatalogueProducts = {
        bakedbeans: 0.99,
        biscuits: 1.2,
        sardines: 1.89,
      }

      const offers: CatalogueOffers = {
        bakedbeans: BUY_TWO_GET_ONE_FREE,
        sardines: TWENTY_FIVE_PERFECT_OFF,
      }

      const superMarket: Catalogue = createCatalogue(products, offers)
      const items: Item[] = basket.getProducts()
      const expected = [
        {
          subtotal: 5.94,
          discount: 1.98,
          total: 3.96,
        },
        {
          subtotal: 3.78,
          discount: 2.83,
          total: 0.95,
        },
      ]
      expect(getPricePerProduce(items, superMarket)).toEqual(expected)
    })
  })
})
