import { Item, Catalogue } from '../types/types'
import {
  BUY_TWO_GET_ONE_FREE,
  TWENTY_FIVE_PERFECT_OFF,
} from '../util/constants'
import { roundPrice } from '../util/priceUtil'

const getProductName = (productName: string) =>
  productName.toLowerCase().replace(' ', '')

const calculateQuanitity = (quantity: number): number => {
  if (quantity <= 1) return 0
  if (quantity > 3 && quantity % 2 === 0)
    return 1 + calculateQuanitity(quantity - 1)
  return calculateQuanitity(quantity - 1)
}

// TODO - refactor to make this dynamically apply offers
const applyOffer = (offerType: string, quantity: number, price: number) => {
  let discountPrice = 0
  if (offerType === BUY_TWO_GET_ONE_FREE) {
    const discounted = calculateQuanitity(quantity)
    discountPrice = (quantity - discounted) * price
  }
  if (offerType === TWENTY_FIVE_PERFECT_OFF) {
    discountPrice = price * 0.25 * quantity
  }
  return roundPrice(discountPrice)
}

const calculateDiscount = (
  subtotal: number,
  discountFunc: Function,
  {
    offerType,
    quantity,
    price,
  }: { offerType: string; quantity: number; price: number }
) => subtotal - discountFunc(offerType, quantity, price)

/**
 * Get all the price per produce
 * @param items list of Items
 * @param catalogue catalogue with products/offers
 * @returns { subtotal, discount, total }
 */
const getPricePerProduce = (items: Item[], catalogue: Catalogue) => {
  const { products, offers } = catalogue
  return items.map(({ productName, quantity }) => {
    const product: string = getProductName(productName)
    const subtotal: number = products[product] * quantity
    let discount = 0
    // check if the offer exist for the product
    if (offers[product]) {
      discount = roundPrice(
        calculateDiscount(subtotal, applyOffer, {
          offerType: offers[product],
          quantity,
          price: products[product],
        })
      )
    }
    const total = roundPrice(subtotal - discount)
    return {
      subtotal: roundPrice(subtotal),
      discount,
      total,
    }
  })
}

export { applyOffer, getPricePerProduce }
