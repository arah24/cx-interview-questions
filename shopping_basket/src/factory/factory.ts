import { CatalogueProducts, CatalogueOffers, Item } from '../types/types'
/**
 * A factory function file - returns objects based on the factory function.
 */

/**
 * Creates a basket that allows you to add an item to the basket.
 * @returns { addProducts(item), getProducts()}
 */
export const createBasket = () => {
  const basket: Item[] = []
  return {
    addProduct: (item: Item) => basket.push(item),
    getProducts: (): Item[] => basket,
  }
}

/**
 * Creates a catalogue
 * @param products list of products
 * @param offers lists of offers
 * @returns { products, offers}
 */
export const createCatalogue = (
  products: CatalogueProducts,
  offers: CatalogueOffers
) => ({
  products,
  offers,
})
