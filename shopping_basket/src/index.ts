import { createBasket, createCatalogue } from './factory/factory'
import { calculatePrice } from './service/basketPricer'
import { getPricePerProduce } from './service/priceService'
import { CatalogueProducts, CatalogueOffers, Catalogue } from './types/types'
import { BUY_TWO_GET_ONE_FREE, TWENTY_FIVE_PERFECT_OFF } from './util/constants'

const basket = createBasket()
basket.addProduct({ productName: 'Baked Beans', quantity: 5 })
basket.addProduct({ productName: 'Sardines', quantity: 4 })
basket.addProduct({ productName: 'Biscuits', quantity: 2 })

const products: CatalogueProducts = {
  bakedbeans: 0.99,
  biscuits: 1.2,
  sardines: 1.89,
}

const offers: CatalogueOffers = {
  bakedbeans: BUY_TWO_GET_ONE_FREE,
  sardines: TWENTY_FIVE_PERFECT_OFF,
  biscuits: BUY_TWO_GET_ONE_FREE,
}

const superMarket: Catalogue = createCatalogue(products, offers)

// inject the function into the calculatePrice function
const result = calculatePrice(getPricePerProduce, {
  basket,
  catalogue: superMarket,
})

console.log(result)
