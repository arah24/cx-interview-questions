export const priceFormat = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'GBP',
})

export const roundPrice = (price: number): number =>
  Math.round(price * 100) / 100
