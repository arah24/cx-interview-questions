export interface Item {
  productName: string
  quantity: number
}

export interface Basket {
  addProduct: Function
  getProducts: Function
}

export interface CatalogueProducts {
  [productName: string]: number
}

export interface CatalogueOffers {
  [productName: string]: string
}

export interface Catalogue {
  products: CatalogueProducts
  offers: CatalogueOffers
}

export interface Result {
  subtotal: string
  discount: string
  total: string
}
